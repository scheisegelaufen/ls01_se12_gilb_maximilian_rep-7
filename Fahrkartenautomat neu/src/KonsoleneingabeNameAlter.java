import java.util.Scanner; // Import der Klasse Scanner

public class KonsoleneingabeNameAlter {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie Ihren Namen ein: ");
		
		String name = myScanner.next();
		
		System.out.print("Bitte geben Sie Ihr Alter ein: ");
		
		int alter = myScanner.nextInt();
		
		
		System.out.print("Ihr Name ist: " + name);
		System.out.print("\nSie sind " + alter + " Jahre alt.");
		
		myScanner.close();

	}

}
