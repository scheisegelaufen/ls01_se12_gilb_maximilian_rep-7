
public class AusgabeFormatierung2 {

	public static void main(String[] args) {
		
		//------------------------Aufgabe 1------------------------		
		System.out.print("Aufgabe 1: \n \n");
		
		System.out.printf("%4s", "*");
		System.out.printf("%-4s \n", "*");
		System.out.printf("%-4s", "*");
		System.out.printf("%4s\n", "*");
		System.out.printf("%-4s", "*");
		System.out.printf("%4s\n", "*");
		System.out.printf("%4s", "*");
		System.out.printf("%-4s", "*");
		
		
		System.out.print("\n\n\n");
		
		
		//------------------------Aufgabe 2------------------------
		System.out.print("Aufgabe 2: \n \n");
		
		System.out.printf("%-5s", "0!");
		System.out.printf("%-20s", "=");
		System.out.printf("=");
		System.out.printf("%5s", "1\n");
		
		System.out.printf("%-5s", "1!");
		System.out.printf("%-20s", "= 1");
		System.out.printf("=");
		System.out.printf("%5s", "1\n");
		
		System.out.printf("%-5s", "2!");
		System.out.printf("%-20s", "= 1 * 2");
		System.out.printf("=");
		System.out.printf("%5s", "2\n");
		
		System.out.printf("%-5s", "3!");
		System.out.printf("%-20s", "= 1 * 2 * 3");
		System.out.printf("=");
		System.out.printf("%5s", "6\n");
		
		System.out.printf("%-5s", "4!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4");
		System.out.printf("=");
		System.out.printf("%5s", "24\n");
		
		System.out.printf("%-5s", "5!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4 * 5");
		System.out.printf("=");
		System.out.printf("%5s", "120\n");
		
		
		System.out.print("\n\n");
		
		
		//------------------------Aufgabe 3------------------------
		System.out.print("Aufgabe 3:\n\n");
		
		int Fahrenheit1 = -20;
		int Fahrenheit2 = -10;
		int Fahrenheit3 = 0;
		int Fahrenheit4 = 20;
		int Fahrenheit5 = 30;
		
		double Celsius1 = -28.8889;
		double Celsius2 = -23.3333;
		double Celsius3 = -17.7778;
		double Celsius4 = -6.6667;
		double Celsius5 = -1.1111;
		
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s%n", "Celsius");
		
		System.out.printf("%-1s%n", "-----------------------");
		
		System.out.printf("%-12s", Fahrenheit1);
		System.out.print("|");
		System.out.printf("%10s", Celsius1 + "\n");
		
		System.out.printf("%-12s", Fahrenheit2);
		System.out.print("|");
		System.out.printf("%10s%n", Celsius2);
		
		System.out.printf("%-12s", Fahrenheit3);
		System.out.print("|");
		System.out.printf("%10s%n", Celsius3);
		
		System.out.printf("%-12s", Fahrenheit4);
		System.out.print("|");
		System.out.printf("%10s%n", Celsius4);
		
		System.out.printf("%-12s", Fahrenheit5);
		System.out.print("|");
		System.out.printf("%10s%n", Celsius5);
		
		//stage-Kommentar, bitte ignorieren
		
	}

}
