
public class VariabelnVerwenden {

	public static void main(String[] args) {
		
		/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		int zaehler ;

		/* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		zaehler = 25;
		System.out.println("Programmdurchl�ufe" + zaehler);

		/* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
		char auswahl ;

		/* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		auswahl = 'c';
		System.out.println("Auswahlbuchstabe f�r das Men�: " + auswahl);

		/* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		long astronomisch ;

		/* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
		astronomisch = 300000;
		System.out.println("Die Lichtgeschwindigkeit ist " + astronomisch + " km/s");

		/* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
		byte anzahlMitglieder = 7;

		/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		System.out.println("Der Verein hat " + anzahlMitglieder + "Mitglieder");

		/* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		float ladung = 0.0000000000000000001602f; 
		System.out.println("Die elektrische Elementarladung betr�gt " + ladung + " As");

		/*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
		boolean erfolgt;

		/*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		erfolgt = true ;
		System.out.println(erfolgt);

	}

}
